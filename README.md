GIT para Ascentio
=======

Repositorio utilizado para la demostración del uso de GIT en la charla de Ascentio 

Clonar repositorio por linea de comando
------------------
Para clonar un repositorio debemos ubicarnos en la carpeta en donde deseamos clonar el mismo y desde el bash de git insertamos la siguiente línea
> git clone https://<user\_name\>@bitbucket.org/amuract_SU/ejemplo-ascentio.git <dir\_name\>
>

el <dir\_name\> es opcional. 
